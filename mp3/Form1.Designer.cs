﻿namespace mp3
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.trackBar = new System.Windows.Forms.TrackBar();
            this.volumeBar = new System.Windows.Forms.TrackBar();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.timeLabel = new System.Windows.Forms.Label();
            this.trackName = new System.Windows.Forms.Label();
            this.artistName = new System.Windows.Forms.Label();
            this.albumName = new System.Windows.Forms.Label();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.prevButton = new System.Windows.Forms.Button();
            this.labelBox = new System.Windows.Forms.PictureBox();
            this.nextButton = new System.Windows.Forms.Button();
            this.stopButton = new System.Windows.Forms.Button();
            this.pauseButton = new System.Windows.Forms.Button();
            this.playButton = new System.Windows.Forms.Button();
            this.openButton = new System.Windows.Forms.Button();
            this.trackList = new System.Windows.Forms.ListView();
            this.durationColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.trackNameColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.artistColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.albumColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label5 = new System.Windows.Forms.Label();
            this.doButton = new System.Windows.Forms.Button();
            this.searchBox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.volumeBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelBox)).BeginInit();
            this.SuspendLayout();
            // 
            // trackBar
            // 
            this.trackBar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.trackBar.Location = new System.Drawing.Point(13, 101);
            this.trackBar.Name = "trackBar";
            this.trackBar.Size = new System.Drawing.Size(391, 45);
            this.trackBar.TabIndex = 0;
            this.trackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBar.Scroll += new System.EventHandler(this.trackBar_Scroll);
            // 
            // volumeBar
            // 
            this.volumeBar.Location = new System.Drawing.Point(259, 157);
            this.volumeBar.Maximum = 100;
            this.volumeBar.Name = "volumeBar";
            this.volumeBar.Size = new System.Drawing.Size(145, 45);
            this.volumeBar.TabIndex = 7;
            this.volumeBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.volumeBar.Scroll += new System.EventHandler(this.volumeBar_Scroll);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(147, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Track Name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(147, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Artist:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(147, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Album:";
            // 
            // timeLabel
            // 
            this.timeLabel.AutoSize = true;
            this.timeLabel.Location = new System.Drawing.Point(317, 85);
            this.timeLabel.Name = "timeLabel";
            this.timeLabel.Size = new System.Drawing.Size(34, 13);
            this.timeLabel.TabIndex = 14;
            this.timeLabel.Text = "00:00";
            // 
            // trackName
            // 
            this.trackName.AutoSize = true;
            this.trackName.Location = new System.Drawing.Point(223, 11);
            this.trackName.Name = "trackName";
            this.trackName.Size = new System.Drawing.Size(10, 13);
            this.trackName.TabIndex = 15;
            this.trackName.Text = "-";
            // 
            // artistName
            // 
            this.artistName.AutoSize = true;
            this.artistName.Location = new System.Drawing.Point(186, 36);
            this.artistName.Name = "artistName";
            this.artistName.Size = new System.Drawing.Size(10, 13);
            this.artistName.TabIndex = 16;
            this.artistName.Text = "-";
            // 
            // albumName
            // 
            this.albumName.AutoSize = true;
            this.albumName.Location = new System.Drawing.Point(186, 62);
            this.albumName.Name = "albumName";
            this.albumName.Size = new System.Drawing.Size(10, 13);
            this.albumName.TabIndex = 17;
            this.albumName.Text = "-";
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "next.png");
            this.imageList1.Images.SetKeyName(1, "fast44.png");
            this.imageList1.Images.SetKeyName(2, "folder.png");
            this.imageList1.Images.SetKeyName(3, "music170.png");
            this.imageList1.Images.SetKeyName(4, "pause.png");
            this.imageList1.Images.SetKeyName(5, "play.png");
            this.imageList1.Images.SetKeyName(6, "prev.png");
            this.imageList1.Images.SetKeyName(7, "rewind44.png");
            this.imageList1.Images.SetKeyName(8, "stop.png");
            this.imageList1.Images.SetKeyName(9, "vol.png");
            this.imageList1.Images.SetKeyName(10, "mute.png");
            this.imageList1.Images.SetKeyName(11, "23811.png");
            this.imageList1.Images.SetKeyName(12, "search.png");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(192, 192);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "Track List";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(235, 157);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(32, 32);
            this.pictureBox1.TabIndex = 19;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // prevButton
            // 
            this.prevButton.ImageIndex = 6;
            this.prevButton.ImageList = this.imageList1;
            this.prevButton.Location = new System.Drawing.Point(150, 153);
            this.prevButton.Name = "prevButton";
            this.prevButton.Size = new System.Drawing.Size(32, 32);
            this.prevButton.TabIndex = 10;
            this.prevButton.UseVisualStyleBackColor = true;
            this.prevButton.Click += new System.EventHandler(this.prevButton_Click);
            // 
            // labelBox
            // 
            this.labelBox.Image = global::mp3.Properties.Resources._23811;
            this.labelBox.Location = new System.Drawing.Point(22, 12);
            this.labelBox.Name = "labelBox";
            this.labelBox.Size = new System.Drawing.Size(100, 83);
            this.labelBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.labelBox.TabIndex = 9;
            this.labelBox.TabStop = false;
            // 
            // nextButton
            // 
            this.nextButton.ImageIndex = 0;
            this.nextButton.ImageList = this.imageList1;
            this.nextButton.Location = new System.Drawing.Point(179, 153);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(32, 32);
            this.nextButton.TabIndex = 6;
            this.nextButton.UseVisualStyleBackColor = true;
            this.nextButton.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // stopButton
            // 
            this.stopButton.ImageIndex = 8;
            this.stopButton.ImageList = this.imageList1;
            this.stopButton.Location = new System.Drawing.Point(118, 153);
            this.stopButton.Name = "stopButton";
            this.stopButton.Size = new System.Drawing.Size(32, 32);
            this.stopButton.TabIndex = 5;
            this.stopButton.UseVisualStyleBackColor = true;
            this.stopButton.Click += new System.EventHandler(this.stopButton_Click);
            // 
            // pauseButton
            // 
            this.pauseButton.ImageIndex = 4;
            this.pauseButton.ImageList = this.imageList1;
            this.pauseButton.Location = new System.Drawing.Point(86, 153);
            this.pauseButton.Name = "pauseButton";
            this.pauseButton.Size = new System.Drawing.Size(32, 32);
            this.pauseButton.TabIndex = 4;
            this.pauseButton.UseVisualStyleBackColor = true;
            this.pauseButton.Click += new System.EventHandler(this.pauseButton_Click);
            // 
            // playButton
            // 
            this.playButton.ImageIndex = 5;
            this.playButton.ImageList = this.imageList1;
            this.playButton.Location = new System.Drawing.Point(54, 153);
            this.playButton.Name = "playButton";
            this.playButton.Size = new System.Drawing.Size(32, 32);
            this.playButton.TabIndex = 3;
            this.playButton.UseVisualStyleBackColor = true;
            this.playButton.Click += new System.EventHandler(this.playButton_Click);
            // 
            // openButton
            // 
            this.openButton.BackColor = System.Drawing.SystemColors.Control;
            this.openButton.ImageIndex = 2;
            this.openButton.ImageList = this.imageList1;
            this.openButton.Location = new System.Drawing.Point(22, 153);
            this.openButton.Name = "openButton";
            this.openButton.Size = new System.Drawing.Size(32, 32);
            this.openButton.TabIndex = 2;
            this.openButton.UseVisualStyleBackColor = false;
            this.openButton.Click += new System.EventHandler(this.openButton_Click);
            // 
            // trackList
            // 
            this.trackList.AllowDrop = true;
            this.trackList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trackList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(197)))), ((int)(((byte)(65)))));
            this.trackList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.trackList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.durationColumn,
            this.trackNameColumn,
            this.artistColumn,
            this.albumColumn});
            this.trackList.FullRowSelect = true;
            this.trackList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.trackList.Location = new System.Drawing.Point(12, 208);
            this.trackList.Name = "trackList";
            this.trackList.Size = new System.Drawing.Size(392, 270);
            this.trackList.TabIndex = 21;
            this.trackList.UseCompatibleStateImageBehavior = false;
            this.trackList.View = System.Windows.Forms.View.Details;
            this.trackList.DragDrop += new System.Windows.Forms.DragEventHandler(this.trackList_DragDrop);
            this.trackList.DragEnter += new System.Windows.Forms.DragEventHandler(this.trackList_DragEnter);
            this.trackList.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.trackList_MouseDoubleClick);
            // 
            // durationColumn
            // 
            this.durationColumn.Text = "Durration";
            this.durationColumn.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // trackNameColumn
            // 
            this.trackNameColumn.Text = "Name";
            this.trackNameColumn.Width = 150;
            // 
            // artistColumn
            // 
            this.artistColumn.DisplayIndex = 3;
            this.artistColumn.Text = "Artist";
            this.artistColumn.Width = 100;
            // 
            // albumColumn
            // 
            this.albumColumn.DisplayIndex = 2;
            this.albumColumn.Text = "Album";
            this.albumColumn.Width = 65;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 489);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 23;
            this.label5.Text = "Search: ";
            // 
            // doButton
            // 
            this.doButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.doButton.ImageIndex = 12;
            this.doButton.ImageList = this.imageList1;
            this.doButton.Location = new System.Drawing.Point(217, 479);
            this.doButton.Name = "doButton";
            this.doButton.Size = new System.Drawing.Size(32, 32);
            this.doButton.TabIndex = 24;
            this.doButton.UseVisualStyleBackColor = true;
            this.doButton.Click += new System.EventHandler(this.doButton_Click);
            // 
            // searchBox
            // 
            this.searchBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.searchBox.Location = new System.Drawing.Point(54, 486);
            this.searchBox.Name = "searchBox";
            this.searchBox.Size = new System.Drawing.Size(157, 20);
            this.searchBox.TabIndex = 25;
            this.searchBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.searchBox_KeyPress);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(197)))), ((int)(((byte)(65)))));
            this.ClientSize = new System.Drawing.Size(416, 513);
            this.Controls.Add(this.searchBox);
            this.Controls.Add(this.doButton);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.trackList);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.albumName);
            this.Controls.Add(this.artistName);
            this.Controls.Add(this.trackName);
            this.Controls.Add(this.timeLabel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.prevButton);
            this.Controls.Add(this.labelBox);
            this.Controls.Add(this.volumeBar);
            this.Controls.Add(this.nextButton);
            this.Controls.Add(this.stopButton);
            this.Controls.Add(this.pauseButton);
            this.Controls.Add(this.playButton);
            this.Controls.Add(this.openButton);
            this.Controls.Add(this.trackBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Bazinga\'s Music Player";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.trackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.volumeBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TrackBar trackBar;
        private System.Windows.Forms.Button openButton;
        private System.Windows.Forms.Button playButton;
        private System.Windows.Forms.Button pauseButton;
        private System.Windows.Forms.Button stopButton;
        private System.Windows.Forms.Button nextButton;
        private System.Windows.Forms.TrackBar volumeBar;
        private System.Windows.Forms.PictureBox labelBox;
        private System.Windows.Forms.Button prevButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label timeLabel;
        private System.Windows.Forms.Label trackName;
        private System.Windows.Forms.Label artistName;
        private System.Windows.Forms.Label albumName;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView trackList;
        private System.Windows.Forms.ColumnHeader durationColumn;
        private System.Windows.Forms.ColumnHeader albumColumn;
        private System.Windows.Forms.ColumnHeader trackNameColumn;
        private System.Windows.Forms.ColumnHeader artistColumn;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button doButton;
        private System.Windows.Forms.TextBox searchBox;
    }
}

