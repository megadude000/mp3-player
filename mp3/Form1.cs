﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using WMPLib;
using System.Windows.Media;
using System.Linq;

namespace mp3
{
    public partial class Form1 : Form
    {
        bool mute = false;              //  mute trigger
        double time = 0.0;              //  remember time value when pause
        bool paused = false;            //  pause trigger
        int volume = 0;                 //  remember volume value
        bool reset = false;             //  list.cleat trigger (dragdrop)
        bool t_enterpressed = false;    //  for search

        PlayList pl = new PlayList(); 

        WMPLib.WindowsMediaPlayer wplayer = new WMPLib.WindowsMediaPlayer();            //  create player interface
        static System.Windows.Forms.Timer myTimer = new System.Windows.Forms.Timer();   //  create timer for refreshing time label

        public Form1()
        {
            InitializeComponent();
        }

        private void openButton_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                System.IO.DirectoryInfo info = new System.IO.DirectoryInfo(folderBrowserDialog1.SelectedPath);

                if (info.Exists)
                {
                    var bw1 = new BackgroundWorker();
                    bw1.DoWork += (senderbw1, args) =>
                    {
                        pl.infoList.Clear();                            //  clear list(if used)
                        pl.DirSearch(info);                             //  scan directory
                    };
                    bw1.RunWorkerCompleted += (senderbw1, args) =>
                    {
                        if (args.Error != null)                         // if an exception occurred during DoWork,
                        {
                            MessageBox.Show(args.Error.ToString());     // do your error handling here
                        }
                        //  what to do after compliting 
                        RefreshTrackList(pl.infoList);
                    };
                    bw1.RunWorkerAsync(); // starts the background worker
                }
            }
        }

        private void RefreshTrackList(List<PlayList.trackInfo> list)
        {
            ListViewItem.ListViewSubItem[] subItems;
            ListViewItem item = null;

            trackList.Items.Clear();
            trackList.Update();
            foreach (PlayList.trackInfo file in list)                   //  filling trackList
            {
                item = new ListViewItem(file.trackDur, 0);
                subItems = new ListViewItem.ListViewSubItem[]
                            {
                                new ListViewItem.ListViewSubItem(item, file.trackName),      // track name
                                new ListViewItem.ListViewSubItem(item, file.artistName),     // artist name
                                new ListViewItem.ListViewSubItem(item, file.albumName)       // album name
                            };
                item.SubItems.AddRange(subItems);
                trackList.Items.Add(item);
            }
        }

        private void playButton_Click(object sender, EventArgs e)
        {
            int sel = Convert.ToInt32(trackList.SelectedIndices[0]);    //  get selected item (if selected > 1 -> take first selected)

            if (paused == false)                            //  play from the begining
            {
                Play();
            }
            else                                            //  if paused - resume
            {
                wplayer.controls.currentPosition = time;
                wplayer.controls.play();
                paused = false;
            }
        }

        private void pauseButton_Click(object sender, EventArgs e)
        {
            if (paused == false)        //  pause
            {
                time = wplayer.controls.currentPosition;    //  remember current position(time)
                paused = true;                              //  set pause trigger
                wplayer.controls.pause();                   //  pause
            }
            else                        //  unpause                
            {
                wplayer.controls.currentPosition = time;    //  set track time
                wplayer.controls.play();                    //  resume
                paused = false;                             //  set pause trigger
            }
        }

        private void stopButton_Click(object sender, EventArgs e)
        {
            wplayer.controls.stop();
        }

        private void pictureBox1_Click(object sender, EventArgs e)  //  volum picture
        {
            if (mute == false)                              //  if unmuted
            {
                volume = wplayer.settings.volume;           //  remember volume value
                wplayer.settings.volume = 0;                //  mute
                pictureBox1.Image = imageList1.Images[10];  //  change picture
                volumeBar.Value = 0;                        //  change volumeBar(tracker) position
                mute = true;                                //  muted
            }
            else                                            //  if muted
            {
                wplayer.settings.volume = volume;           //  set volume value
                volumeBar.Value = volume;                   //  change volumeBar(tracker) position
                pictureBox1.Image = imageList1.Images[9];   //  change picture
                mute = false;                               //  resumed
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            wplayer.settings.volume = 50;                           //  set start volume value
            volumeBar.Value = 50;                                   //  set start volumeBar(tracker) position
            pictureBox1.Image = imageList1.Images[9];               //  set volume picture(unmuted)
            myTimer.Tick += new EventHandler(TimerEventProcessor);  //  create timer
            myTimer.Interval = 1000;                                //  refresh time label & trackBar every second
        }

        private void Play()
        {
            int sel = Convert.ToInt32(trackList.SelectedIndices[0]);    //  get selected item (if selected items > 1 -> take first selected)
            trackList.Items[sel].Selected = true;                       //  select current selected item
            trackList.Select();

            try
            {
                string file = pl.infoList[Convert.ToInt32(trackList.SelectedIndices[0])].fullTrackName; //  get path to file
                wplayer.URL = file;                                                                     //  add file to player
                wplayer.controls.play();                                                                //  play on
                myTimer.Start();                                                                        //  timer - on
                wplayer.currentMedia.name = wplayer.currentMedia.name;                                  //  for some reasons but it's necessary
                SetLable(file);                                                                         //  set info to main screen
                trackBar.Maximum = Convert.ToInt32(wplayer.controls.currentItem.duration);              //  track dur = max trackBar
            }
            catch (System.Exception ec)
            {
                MessageBox.Show("Shish! Something goes wrong");
            }
        }

        private void SetLable(string path)
        {
            TagLib.File tagFile = TagLib.File.Create(path);
            artistName.Text = tagFile.Tag.FirstAlbumArtist;
            albumName.Text = tagFile.Tag.Album;
            trackName.Text = tagFile.Tag.Title;

            //  set track label to labelBox
            //  load image to MemoryStream
            if (tagFile.Tag.Pictures != null && tagFile.Tag.Pictures.Length != 0)   //  if label exists
            {
                var bin = (byte[])(tagFile.Tag.Pictures[0].Data.Data);
                labelBox.Image = Image.FromStream(new MemoryStream(bin));
            }
            else                                                                    //  if not
            {
                labelBox.Image = Properties.Resources._23811;                       //  set default pic
                labelBox.Refresh();
            } 
        }

        private void PlayNext()
        {
            if (trackList.SelectedItems.Count > 0)
            {
                int sel = Convert.ToInt32(trackList.SelectedIndices[0]);

                if ((sel + 1) < trackList.Items.Count)            //  if not end of trackList
                {
                    trackList.Items[sel].Selected = false;
                    trackList.Items[sel + 1].Selected = true;
                    trackList.Select();
                    Play();
                }
                else                                            //  go to begining
                {
                    trackList.Items[0].Selected = true;
                    trackList.Select();
                    Play();
                }
            }
        }

        private void PlayPrev()
        {

            if (trackList.SelectedItems.Count > 0)
            {
                int sel = Convert.ToInt32(trackList.SelectedIndices[0]);

                if ((sel - 1) > -1)                             //  can go over?
                {
                    trackList.Items[sel].Selected = false;
                    trackList.Items[sel - 1].Selected = true;
                    Play();
                }
                else                                            //  if not - play on first element
                {
                    Play();
                }
            }
        }

        private void volumeBar_Scroll(object sender, EventArgs e)
        {
            wplayer.settings.volume = volumeBar.Value;      //  change volume value
        }

        private void TimerEventProcessor(Object myObject, EventArgs myEventArgs)
        {
            ChangeTimeLable();
        }
                                            //                      total /current
        private void ChangeTimeLable()      //  set time label to [h:mm:ss/h:mm:ss] type
        {
            int max = Convert.ToInt32(wplayer.controls.currentItem.duration); max -= 2;
            int temp = Convert.ToInt32(wplayer.controls.currentPosition);
            string dur = "";
            int sec = 0;
            int min = 0;

            if (trackList.SelectedItems.Count > 0)
            {
                dur = pl.infoList[Convert.ToInt32(trackList.SelectedIndices[0])].trackDur;  //  get track durration
            }

            if (temp > max) //  if almoust end -> play next
            {
                PlayNext();
            }

            try
            {
                trackBar.Value = temp;  //  set current time to trackBar
            }
            catch (System.Exception ec)
            {
                //ssss
            }

            if (temp < 10)
            {
                timeLabel.Text = dur + "/0:00:0" + temp.ToString();
            }
            if (temp >= 10 && temp < 59)
            {
                timeLabel.Text = dur + "/0:00:" + temp.ToString();
            }
            if (temp >= 60)
            {
                while (temp > 60)
                {
                    min++;
                    temp -= 60;
                }
                sec = temp;
                if (sec < 10 && min < 10)
                {
                    timeLabel.Text = dur + "/0:0" + min.ToString() + ":0" + sec.ToString();
                }
                if (sec >= 10 && sec < 59 && min < 10)
                {
                    timeLabel.Text = dur + "/0:0" + min.ToString() + ":" + sec.ToString();
                }

                if (sec < 10 && min >= 10)
                {
                    timeLabel.Text = dur + "/0:" + min.ToString() + ":0" + sec.ToString();
                }
                if (sec >= 10 && sec < 59 && min >= 10)
                {
                    timeLabel.Text = dur + "/0:" + min.ToString() + ":" + sec.ToString();
                }
            }
        }

        private void trackBar_Scroll(object sender, EventArgs e)                    //  rewind
        {
            wplayer.controls.currentPosition = Convert.ToDouble(trackBar.Value);    //  set current track time relatively to trackBar
            wplayer.controls.play();
        }

        private void nextButton_Click(object sender, EventArgs e)
        {
            PlayNext();
        }

        private void prevButton_Click(object sender, EventArgs e)
        {
            PlayPrev();
        }

        private void trackList_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Play();
        }

        private void trackList_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])(e.Data.GetData(DataFormats.FileDrop, false));       //  get directory/files name

            var bw1 = new BackgroundWorker();
            bw1.DoWork += (senderbw1, args) =>
            {
                foreach (string file in files)
                {
                    try
                    {
                        System.IO.DirectoryInfo info = new System.IO.DirectoryInfo(file);   //  if it's directory - search it
                        if (Directory.Exists(file))
                        {
                            if (reset == false)                                             //  if list aint cleared
                            {
                                pl.infoList.Clear();                                        //  clear
                                reset = true;
                            }
                            pl.DirSearch(info);                                             //  search in directory
                        }
                    }
                    catch (System.Exception ec)
                    {
                        //ssss
                    }
                   
                    string ext = Path.GetExtension(file);
                    if (ext == ".mp3" || ext == ".m4a" || ext == ".wav")                    //  if it's file - add to list                   
                    {
                        if (reset == false)                                                 //  if list aint cleared
                        {
                            pl.infoList.Clear();                                            //  clear
                            reset = true;
                        }
                        pl.AddToInfoList(Path.GetFullPath(file).ToString());                    //  add to list
                    }
                }
            };
            bw1.RunWorkerCompleted += (senderbw1, args) =>
            {
                if (args.Error != null)                         // if an exception occurred during DoWork,
                {
                    MessageBox.Show(args.Error.ToString());     // error handling here
                } 
                reset = false;                                  //  set trigger for next use
                RefreshTrackList(pl.infoList);                             //  refresh trackList
            };
            bw1.RunWorkerAsync(); // starts the background worker
        }

        private void trackList_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
        }

        private void doButton_Click(object sender, EventArgs e)
        {
            StartSearch();              //  search it!
            t_enterpressed = true;
        }

        private void StartSearch()
        {
            if (searchBox.Text != null)     //  If there is something
            {
                var bw1 = new BackgroundWorker();
                bw1.DoWork += (senderbw1, args) =>
                {
                    pl.searchResultList.Clear();                           //  clear list(if used)
                    pl.Search(searchBox.Text);                             //  search sim...
                };
                bw1.RunWorkerCompleted += (senderbw1, args) =>
                {
                    if (args.Error != null)                         // if an exception occurred during DoWork,
                    {
                        MessageBox.Show(args.Error.ToString());     // do your error handling here
                    }
                    //  what to do after compliting 
                    RefreshTrackList(pl.searchResultList);
                };
                bw1.RunWorkerAsync(); // starts the background worker
            }
        }

        private void searchBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)              //  enter press
            {
                StartSearch();
                t_enterpressed = true;
            }

            if (e.KeyChar == (char)Keys.Delete)     //  del press
            {
                if (t_enterpressed == true)         //  if it's first del after sersh
                {
                    RefreshTrackList(pl.infoList);  //  set infoList to trackList (coz default)
                    t_enterpressed = false;         //  & set trigger as negative
                }
            }

            if (e.KeyChar == (char)Keys.Back)       //  backspace press
            {
                if (t_enterpressed == true)
                {
                    RefreshTrackList(pl.infoList);
                    t_enterpressed = false;
                }
            }
        }
    }
}
